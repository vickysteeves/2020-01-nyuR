# NYU Data Carpentries Lesson - January 2020

## About
This repository holds materials for the R for Social Science lesson taught at NYU at a Data Carpentry workshop in January 2020.

## Build this book locally
1. Fork, clone or download this project.
2. Make sure you have R & RStudio installed.
3. Install the bookdown, RMarkdown, tidyverse, and tinytex packages in RStudio with the following two commands in the R terminal:
	* `install.packages(c("rmarkdown", "bookdownplus", "tinytex", "tidyverse"))`
	* `tinytex::install_tinytex()`
	You can also click Tools > Install Packages and type the package names (make sure "install dependencies" is checked) separated by commas.
4. Go to the project folder and click `2020-01-nyuR.Rproj`
5. Run this command in the R terminal: `bookdown::render_book('index.Rmd', 'all')`
6. Go to the folder `_book` in the project folder and click `index.html` to view the book locally in your browser.

## Contact info
You are welcome to email me at [vicky dot steeves at nyu dot edu](mailto:vicky.steeves@nyu.edu) if you have questions or concerns, or raise an issue on this repository and I will do my best to respond quickly!