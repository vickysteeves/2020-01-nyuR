# Getting Started with R

## Variables {-}
Before we do all these fancy operations with our newly downloaded data, let's first understand how R does things. Like most programming languages, you can do math right from R.

In the Console (bottom-right), just try typing `4*20` and hit enter. You should get `80` as your output. Great!

However, it's more useful for the sake of our purposes (research!) to save values and reuse them over time. Like Alex said, let the computer do as much work as we can get it to!

R is different from most other programming languages, because it assigns values to variables with this: `<-`. In your R script, write out and run:

```{r}
# assign a value to area_hectares
area_hectares <- 1.0 

# print out area_hectares
area_hectares 
```

Afer it runs, you should see 1.0. That's because in R, you put the name of the variable first, add the `<-`, and then the value that you want the variable to hold. If you were to just write out `area_hectares <- 1.0`, nothing would print, because all you've done is assign the value to the variable, not actually called it! That's why we add it again in the line below.

You might have also noticed some text following the `#` symbol. This is a comment! These are useful to include in your scripts because they tell you *exactly* what you're doing in plain human language (not scripting language). This will be ignored by R and just for our benefit.

Now that we have a variable, we can do math with it! Try typing this out in your script and running it:

```{r}
2.47 * area_hectares
```

Be careful with your variables, because you can overwrite their meaning by just assigning it another value, like so (run this in your Rs script!):

```{r}
area_hectares <- 2.5
```

We can also create new variables by performing operations on other ones! Try running this in your script:

```{r}
area_acres <- 2.47 * area_hectares
```

Since we created the new variable `area_acres`, we can also do whatever we want to it! Like this (run this in your R Script):

```{r}
area_hectares <- 50
```

**EXERCISE**

What do you think is the current content of the variable `area_acres`? 123.5 or 6.175? Try running it in your R script to give me the answer.

Put up a blue stickie when you have the answer!

**EXERCISE**

Create two variables `length` and `width` and assign them values. It should be noted that R Studio might add "()" after width and if you leave the parentheses you will get unexpected results. This is why you might see other programmers abbreviate common words. Create a third variable `area` and give it a value based on the current values of length and width. Show that changing the values of either length and width does not affect the value of area.

Put up a blue stickie when you're done!

## Functions {-}
One of the best things about using *any* programming language are **functions**. Functions are like canned operations that can be used by anyone who installs a language that automate more complicated commands. Most of these are pre-defined (e.g. that come with R alone) and some are added by additional packages (like the functions that come with `tidyverse` that we'll see later!).

Let's try out another math function in our R script:

```{r}
# make a variable a and give it a value
a <- 64 

# put the square root of a into a new variable, b
b <- sqrt(a) 
```

Here, the value of `a` is given to the `sqrt()` function, which calculates the square root, and returns the value which is then assigned to the variable `b`. This function is very simple, because it takes just one argument (one thing goes in the parens). Some take multiple, like `round()`. Let's try it out in our R script:

```{r}
round(3.14159, digits = 2)
```

By default, `round()` will go to the next whole number (e.g. no decimals) -- but this isn't great for those of us who need those decimals to be accurate in our analysis. So we pass an additional argument to the function -- `digits=2`, which tells round() to go to the 2nd decimal place.

**EXERCISE**

Type in `?round` at the Console (bottom left) and then look at the output in the Help pane (bottom right). Talk at your table about these questions, and then we'll come back to discuss as a group:

* What other functions exist that are similar to round? 
* How do you use the digits parameter in the round function?

## Vectors and Data Types {-}

Give me a blue stickie if you're with me so far, or a red stickie if I'm moving too fast or need me to review anything so far.

Vectors are a foundational data type in R -- it's composed of a series of values. We can assign values using the `c()` function. So let's create a vector of household members that participated in our study and assign it to a new object `hh_members`. Like variables, we do this with `<-`:

```{r}
# create vector
hh_members <- c(3, 7, 10, 6) 

# print vector
hh_members 
```

We've been doing all numbers so far, so I think it's worth noting that we can also have variables and vectors that have characters! Let's create a new vector with characters in our script. For example, we can have a vector of the building material used to construct our interview respondents' walls (`respondent_wall_type`): 

```{r}
# create vector of chars
respondent_wall_type <- c("muddaub", "burntbricks", "sunbricks")

# print vector
respondent_wall_type
```

You **must** have quotes around your characters, or else R will assume that there are variables called `muddaub`, `burntbricks`, and `sunbricks`. Computers only know what we tell them and nothing more or less.

You can also use functions with vectors! One useful function for vectors is `length()`, which gives you the number of elements in a vector. Let's try it in our script:

```{r}
# print the length of hh_members
length(hh_members)

# print the length of respondent_wall_type
length(respondent_wall_type)
```

Ok, another useful function for checking our data is `class()`. This will tell us what *kind* of vector you have, which informs what we can do with it (can't do math on characters!). There are 6 "atomic" vector classes in R:

* `numeric`: numbers with decimals
* `character`: characters in quotes (you can have numbers in quotes and they'll be characters, but you can do less with them that way...)
* `logical`: TRUE and FALSE (boolean data type)
* `integer`: round numbers (no decimals)
* `complex`: like complex numbers, `4+5i`
* `raw` for bitstreams that we won’t discuss further

Let's see what classes our two existing vectors are by adding this into our R script and running it:

```{r}
# see what kind of elements are in the vector
class(hh_members)

# see what kind of elements are in the vector
class(respondent_wall_type)
```

**EXERCISE**

We've seen that vectors can be of type character, numeric (or double), integer, and logical. But what happens if we try to mix these types in a single vector? Try creating a new vector that mixes data type, and then run class() to see what happens.

Put up a blue stickie when you're done and have an answer!

Another important way to to interact with vectors is *subsetting*, or extracting a few values. We do this via square brackets next to the name of the vector, which holds the index of the element we want to extract. R is different from other languages because it starts counting at 1 -- in Python, for example, you count from 0.

Anyway, let's try it! In your R script, type and run:

```{r}
# print a subset of our vector
respondent_wall_type[2] 
```

You can also do this with more than one element, as long as you separate each index with a comma. Let's use this to create a new vector with some repeating values from our original vector:

```{r}
more_respondent_wall_type <- respondent_wall_type[c(1, 2, 3, 2, 1, 3)]
```

You can also do this with *conditional* statements -- like "bigger than" or "equal to" by using the same formula that we'd do when writing that out by hand! Let's create a new integar vector and look for some values:

```{r}
hh_members <- c(3, 7, 10, 6, 2)

# look for all values that are over 5 and print them
hh_members[hh_members > 5] 
```

You'll notice that the answer is given in the order of appearance in the vector.

You can have *multiple* conditionals, if you have a lot of conditions to fulfill! R considers `&` to be the same as "AND", and considers `|` to be the same as "OR". Let's write and run a slightly more complicated conditional:

```{r}
hh_members[hh_members < 3 | hh_members > 5]
```

## Missing Data {-}

Give me a blue stickie if you're with me so far, or a red stickie if I'm moving too fast or need me to review anything so far.

Since R was designed by stats people who had to deal with missing data all the time, it should be no surprise that R has baked into it the concept of missing data (a unique feature!).

In vectors in R, missing data is represented as `NA`. Let's write a new vector with a missing value, and try to do some math on it to see what happens.

```{r}
# create a vector with a missing value
rooms <- c(2, 1, 1, NA, 4) 

# print the mean of the values in the vector
mean(rooms) 
```

Nothing happens! Because we didn't account for our missing value. Let's try again, but add in another argument to tell the `mean()` function what to do with our missing data:

```{r}
# prints the mean of vector and ignores the NAs
mean(rooms, na.rm = TRUE)
```

The argument `na.rm=TRUE` tells the function to calculate the result while ignoring the missing values.

**EXERCISE**

1. Using a new vector, guitars, create a new vector with the NAs removed:
  `guitars <- c(1, 2, 1, 1, NA, 3, 1, 3, 2, 1, 1, 8, 3, 1, NA, 1)`
2. Then use the function median() to calculate the media of the vector.
3. Use R to figure out how many households have more than 2 guitars.

Put up a blue stickie when you have the answer!