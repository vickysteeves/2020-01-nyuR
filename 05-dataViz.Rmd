# Data Visualisation with ggplot2

In case you accidentally exited RStudio, make sure that you load the data we just exported:

```{r}
interviews_plotting <- read_csv("results/interviews_plotting.csv")
```

`ggplot2` is a plotting package that makes it simple to create complex plots from data stored in a data frame. It's by far my favourite way to plot, especially when I am preparing articles for publication. I like it especially because we only need minimal changes if the underlying data change or if we decide to change from a bar plot to a scatterplot. This helps in creating publication quality plots with minimal amounts of adjustments and tweaking -- like Alex's mantra, let the computer do as much work as we can!

To built a ggplot, we use this basic formula:

`ggplot(data = <DATA>, mapping = aes(<X and Y axes>)) +  <GEOM_FUNCTION>()`

"geoms" are graphical representations of data (e.g. points, lines bars). We'll use these today, though there are *many* more:

* `geom_point()`: scatter plots, dot plots, etc.
* `geom_boxplot()`: boxplots!
* `geom_line()`: trend lines, time series, etc.

Let's use this formula and new knowledge of geoms to make a scatter plot with our interview data:

```{r}
ggplot(data = interviews_plotting, aes(x = no_membrs, y = number_items)) +
    geom_point()
```

The `+` in the `ggplot2` package is particularly useful because it allows you to modify existing `ggplot` objects. This means you can easily set up plot templates and conveniently explore different types of plots, so the above plot can also be generated with code like this:

```{r}
# Assign plot to a variable
interviews_plot <- ggplot(data = interviews_plotting, aes(x = no_membrs, y = number_items))

# Draw the plot
interviews_plot + geom_point()
```

We could substitute `geom_point()` for another geom very easily, and get an entirely new plot with just one word change. Nice!

You can pack a lot into a `ggplot` object. Let's look at how to add some color to our plot, starting with a solid color for all the points:

```{r}
ggplot(data = interviews_plotting, aes(x = no_membrs, y = number_items)) +
    geom_point(aes(color = "blue"))
```

Nice! But this isn't extremely descriptive. Let's change the colors based on the villages that each respondent comes from, so we can see the plot is more visually revealing. You might notice that I added the `aes()` function to the ggplot object -- this helps with the aesthetic mappings for every layer. It's a best practice to include this, even if there's only one argument.


```{r}
ggplot(data = interviews_plotting, aes(x = no_membrs, y = number_items)) +
    geom_point(aes(color = village))
```

Notice how `ggplot` also gives us a legend on the right automatically!

**EXERCISE**

Use what you just learned to create a scatter plot of rooms by village with the respondent_wall_type showing in different colors. Is this a good way to show this type of data?

Put up your blue stickie when you've finished!

Maybe a boxplot would be more effective in showing the distribution of rooms for each wall type, for our future readers? Let's try it and see: 

```{r}
ggplot(data = interviews_plotting, aes(x = respondent_wall_type, y = rooms)) +
    geom_boxplot() +
    geom_jitter(aes(color = "#56B4E9"))
```

You may have noticed I used the geom `jitter` this time -- this a convenient shortcut for `geom_point(position = "jitter")`. It adds a small amount of random variation to the location of each point, and is a useful way of handling overplotting caused by discreteness in smaller datasets. You may have noticed that I also used HEX notation `#56B4E9` to get the color of the points -- you can either have the name of a color or the HEX, as you want.

**EXERCISES**

1. Boxplots are useful summaries, but hide the shape of the distribution. For example, if the distribution is bimodal, we would not see it in a boxplot. An alternative to the boxplot is the violin plot, where the shape of the density of points is shown. Replace the box plot with a violin plot -- `geom_violin()` and see what happens.

2. Create a boxplot for `liv_count` for each wall type. Overlay the boxplot layer on a jitter layer to show actual measurements.

3. Add color to the data points on your boxplot according to whether the respondent is a member of an irrigation association (`memb_assoc`).

Put up your stickies when you've finished all 3 questions!

Let's look at barchats now -- these are also useful for visualizing categorical data. By default, `geom_bar` accepts a variable for the `x`, and plots the number of instances each value of `x` appears in the dataset. Let's try it with wall types:

```{r}
ggplot(data = interviews_plotting, aes(x = respondent_wall_type)) +
    geom_bar()
```

If you want to stack villages within each of these bars to see the count of how many houses from each village has walls of a given type, we can use the `fill` aesthetic for  `geom_bar()` to color bars by the portion of each count that is from each village: 

```{r}
ggplot(data = interviews_plotting, aes(x = respondent_wall_type)) +
    geom_bar(aes(fill = village))
```

I personally have a hard time reading these, so I use `position = "dodge"` to put them side-by-side instead of on top of each other, like this: 

```{r}
ggplot(data = interviews_plotting, aes(x = respondent_wall_type)) +
    geom_bar(aes(fill = village), position = "dodge")
```

HOWEVER we've been missing a few things this entire time...somethings key to good plots. Titles and labels!! We have the defaults for the X and Y axes, which are column headers, but those aren't the most visually appealing. We can append `labs` to give a value for `title`, `x`, and `y`, which will show up in our plot nicely: 

```{r}
ggplot(data = interviews_plotting, aes(x = respondent_wall_type)) +
    geom_bar(aes(fill = village), position = "dodge") + 
    labs(title="Count of wall type by village",
         x="Wall Type",
         y="Count")
```

Now that we know how to add all the necessary elements, we can get fancy with constructing our plots. We can use some functions from our last lesson to create a new data frame to plot -- for this data, we're more likely to be interested in the proportion of each housing type in each village than in the actual count of number of houses of each type. 

To compare proportions, we will first create a new data frame (`percent_wall_type`) with a new column named "percent" representing the percent of each house type in each village. We will remove houses with cement walls, as there was only one in the dataset.

```{r}
percent_wall_type <- interviews_plotting %>%
    filter(respondent_wall_type != "cement") %>%
    count(village, respondent_wall_type) %>%
    group_by(village) %>%
    mutate(percent = n / sum(n)) %>%
    ungroup()
```

Now that we have the new data frame in shape, let's plot it as a bar plot with titles and labels!!


```{r}
ggplot(percent_wall_type, aes(x = village, y = percent, fill = respondent_wall_type)) +
    geom_bar(stat = "identity", position = "dodge") +
    labs(title="Proportion of wall type by village",
         x="Wall Type",
         y="Percent")
```

**EXERCISE**

1. Look at the ggplot2 cheatsheet [pdf]: https://www.rstudio.com/wp-content/uploads/2016/11/ggplot2-cheatsheet-2.1.pdf

2. With all of the days lessons in hand, either improve one of the plots generated in this exercise or create a beautiful graph of your own.

## Exporting our visualizations {-}

So while we've been making a lot of lovely graphs, we haven't been savely them to `.png` or other image formats -- so we can't insert them into manuscripts or presentations yet!

Let's save a plot using the `ggsave()` function, which lets us easily change the dimension and resolution of our plots. Make sure you save it in the `figures/` folder!

```{r}
my_plot <- ggplot(percent_wall_type, aes(x = village, y = percent, fill = respondent_wall_type)) +
    geom_bar(stat = "identity", position = "dodge") +
    labs(title="Proportion of wall type by village",
         x="Wall Type",
         y="Percent")

ggsave("figures/wall-type-by-village.png", my_plot, width = 15, height = 10, dpi = 300)
```

Note: The parameters `width` and `height` also determine the font size in the saved plot.