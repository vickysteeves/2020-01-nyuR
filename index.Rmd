--- 
title: "R for Social Scientists"
author: "Vicky Steeves"
date: "2020-01-15"
description: "Material for NYU Carpentry Workshop - R for Social Scientists"
site: bookdown::bookdown_site
colorlinks: yes
documentclass: book
always_allow_html: yes
output:
  bookdown::gitbook:
    css: [style.css]
    config:
      toc:
        before: |
          <p><a href="">R for Social Scientists</a></p>
          <p>2020-01-15</p>
          <p>Instructor: <a href="hhttps://library.nyu.edu/people/victoria-steeves/">Vicky Steeves</a></p>
        after: |
          <p><a href="https://gitlab.com/RLesur/bookdown-gitlab-pages" target="blank">Published with bookdown &amp; GitLab</a></p>
          <p>Content is from the Data Carpentries <a href="https://datacarpentry.org/r-socialsci/">R for Social Science</a> lesson.</p>
          <p>Any questions, contact: <a href="mailto:vicky.steeves@nyu.edu">vicky.steeves@nyu.edu</a></p>
      edit: https://gitlab.com/VickySteeves/2020-01-nyuR/edit/master/%s
      search: yes
      download: ["pdf", "epub"]
      sharing: null
  bookdown::pdf_book:
    latex_engine: pdflatex
  bookdown::epub_book: default
---

# Welcome {-}

Welcome to day two of the first Carpentries workshop of 2020! I'm Vicky Steeves, who you all should know by now. A reminder of our schedule:

| TIME  | ACTIVITY                       |
|-------|--------------------------------|
| 09:30 | Begin R for Social Scientists  |
| 11:00 | Morning break                  |
| 11:20 | R for Social Scientists - cont |
| 12:00 | Lunch break                    |
| 13:00 | R for Social Scientists - cont |
| 14:40 | Afternoon break                |
| 15:00 | R for Social Scientists - cont |
| 16:50 | Post-workshop survey           |
| 17:00 | End of the workshop            |

Reminder: blue stickie is "I'm good" and red stickie is "I need help", and you'll be expected to give me some feedback on them before lunch and at the end of the day.

Today we are going to learn about R! R is a programming language that was made by some statisticians who were royally ticked off that its precusor, S, was proprietary and expensive. So they made their OWN language, free and open source! R is partly a play on the name of S, and partly an homage to the two creators, Ross Ihaka and Robert Gentleman.

As you saw in the setup, R and RStudio are separate downloads and installations. R is the underlying programming language, but using R alone can be daunting – we’d have to write our R files in a text editor and then run the scripts in the terminal. RStudio is a graphical integrated development environment (IDE) that makes using R much easier and more interactive. To function correctly, RStudio needs R and therefore both need to be installed on your computer.

So why use R? Well, it's popular among the social sciences and it's a social science workshop. But why did it become so popular? Well:

* It's **not** point and click, which is good for reproducibility! Since programs (not OpenRefine!) don't track everything you do, it's had to replicate someone's workflow if it's all clicking in submenus -- that's also super annoying to write down. 
* Scripts force you to make your analysis **incredibly clear** and facilitates your learning and comprehension of the methods you use.
* The code you write can be inspected by someone else who can give you feedback and spot mistakes (which is harder to do if they have to stand over your shoulder and watch the point/clicks!)
* R has 10,000+ packages, so you can use it to do just about anything you want in whatever discipline you're in!
* R works with whatever size and shape data you have -- 100 or 1 million, it's no different to your script (but you may need a larger computer!).
* R makes **amazing** graphics, including optimizing plots for publication (300dpi!).
* R is free and open source, so you can take this knowledge with you anywhere!